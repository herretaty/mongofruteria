package com.mongo.tatiana.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Util {
    public static LocalDate parsearFecha(String fechaCaducidad) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(fechaCaducidad,formateador);
    }
    public static String formatearFecha(LocalDate fechaMatriculacion){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fechaMatriculacion);

    }
}
