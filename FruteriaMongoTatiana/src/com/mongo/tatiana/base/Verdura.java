package com.mongo.tatiana.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Verdura {

    private ObjectId id;
    private String nombre;
    private String origen;
    private double pesoNeto;
    private LocalDate fechaCaducidad;


    public Verdura(){

     }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public double getPesoNeto() {
        return pesoNeto;
    }

    public void setPesoNeto(double pesoNeto) {
        this.pesoNeto = pesoNeto;
    }

    public LocalDate getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(LocalDate fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }
    @Override
    public String toString() {
        return  nombre + ":" + origen;
    }
}