package com.mongo.tatiana;

import com.mongo.tatiana.gui.Controlador;
import com.mongo.tatiana.gui.Modelo;
import com.mongo.tatiana.gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);

}
}
