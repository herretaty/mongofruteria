package com.mongo.tatiana.gui;

import com.mongo.tatiana.base.Fruta;
import com.mongo.tatiana.base.Verdura;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador  implements ActionListener, KeyListener, ListSelectionListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarFrutas(modelo.getFrutas(vista.txtBuscar.getText()));
        listarVerduras(modelo.getVerduras());
    }

    private void addKeyListeners(KeyListener listener) {
        vista.txtBuscar.addKeyListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listFrutas.addListSelectionListener(listener);
        vista.listVerdura.addListSelectionListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        vista.btnNuevo.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevoVerdura.addActionListener(listener);
        vista.btnModificarVerdura.addActionListener(listener);
        vista.btnEliminarVerdura.addActionListener(listener);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Fruta unaFruta;
        Verdura unaVerdura;
        switch (comando){
            case "Nuevo":
                unaFruta = new Fruta();
                modificarFrutaFromCampos(unaFruta);
                modelo.guardarFruta(unaFruta);
                listarFrutas(modelo.getFrutas(vista.txtBuscar.getText()));
                break;
            case "Modificar":
                unaFruta = (Fruta) vista.listFrutas.getSelectedValue();
                modificarFrutaFromCampos(unaFruta);
                modelo.modificarFruta(unaFruta);

                listarFrutas(modelo.getFrutas(vista.txtBuscar.getText()));
                break;
            case "Eliminar":
                unaFruta = (Fruta) vista.listFrutas.getSelectedValue();
                modelo.borrarFruta(unaFruta);
                listarFrutas(modelo.getFrutas(vista.txtBuscar.getText()));
                break;
            case "Nueva Verdura":
                unaVerdura= new Verdura();
                modificarVerduraFromCampos(unaVerdura);
                modelo.guardarVerdura(unaVerdura);
                listarVerduras(modelo-getVerdura());
                break;
            case "Modificar Verdura":
                unaVerdura= (Verdura)vista.listVerdura.getSelectedValue();
                modificarVerduraFromCampos(unaVerdura);
                modelo.modificarVerdura(unaVerdura);
                listarVerduras(modelo.getVerdura());
                break;
            case "Eliminar Verdura":
                unaVerdura= (Verdura) vista.listVerdura.getSelectedValue();
                modificarVerduraFromCampos(unaVerdura);
                modelo.modificarVerdura(unaVerdura);
                break;
        }
        
    }

    private void listarFrutas(List<Fruta> lista) {
        vista.dlmFrutas.clear();
        for (Fruta fruta : lista){
            vista.dlmFrutas.addElement(fruta);
        }

    }

    private void modificarVerduraFromCampos(Verdura unaVerdura) {
        unaVerdura.setNombre(vista.txtNombreVerdura.getText());
        unaVerdura.setOrigen(vista.txtOrigenVerdura.getText());
        unaVerdura.setPesoNeto(Double.parseDouble(vista.txtPesoVerdura.getText()));
        unaVerdura.setFechaCaducidad(vista.fechaCaducidadVerdura.getDate());
    }

    private void modificarFrutaFromCampos(Fruta unaFruta) {
        unaFruta.setNombre(vista.txtNombreFruta.getText());
        unaFruta.setOrigen(vista.txtOrigen.getText());
        unaFruta.setPesoNeto(Double.parseDouble(vista.txtPesoNeto.getText()));
        unaFruta.setFechaCaducidad(vista.fechaCaducidad.getDate());

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscar){
            listarFrutas(modelo.getFrutas(vista.txtBuscar.getText()));
        }
        if (e.getSource()== vista.txtBuscarVerdura){
            listaVerdura(modelo.getVerduras(vista.txtBuscarVerdura.getText()));
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            Fruta unaFruta= (Fruta) vista.listFrutas.getSelectedValue();
            vista.txtNombreFruta.setText(unaFruta.getNombre());
            vista.txtOrigen.setText(unaFruta.getOrigen());
            vista.txtPesoNeto.setText(String.valueOf(unaFruta.getPesoNeto()));
            vista.fechaCaducidad.setDate(unaFruta.getFechaCaducidad());
        }
        if (e.getValueIsAdjusting()){
            Verdura unaVerdura= (Verdura) vista.listVerdura.getSelectedValue();
            vista.txtNombreVerdura.setText(unaVerdura.getNombre());
            vista.txtOrigenVerdura.setText(unaVerdura.getOrigen());
            vista.txtPesoVerdura.setText(String.valueOf(unaVerdura.getPesoNeto()));
            vista.fechaCaducidadVerdura.setDate(unaVerdura.getFechaCaducidad());

        }

    }
}
