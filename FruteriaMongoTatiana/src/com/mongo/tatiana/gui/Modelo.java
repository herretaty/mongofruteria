package com.mongo.tatiana.gui;

import com.mongo.tatiana.base.Fruta;
import com.mongo.tatiana.base.Verdura;
import com.mongo.tatiana.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {

    private final static String COLECCION_FRUTAS = "Frutas";
    private final static String COLECCION_VERDURAS = "Verduras";
    private final static String DATABASE = "Productos";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionFrutas;
    private MongoCollection coleccionVeruras;

    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionFrutas = baseDatos.getCollection(COLECCION_FRUTAS);
         coleccionVeruras= baseDatos.getCollection(COLECCION_VERDURAS);

    }
    public void desconectar(){
        client.close();

    }

    public void guardarFruta(Fruta unaFruta) {
        coleccionFrutas.insertOne(frutaToDocument(unaFruta));
    }
    public void guardarVerdura(Verdura unaVerdura) {
        coleccionVeruras.insertOne(verduraToDocument(unaVerdura));
    }

    public List<Fruta> getFrutas(){
        ArrayList<Fruta> lista = new ArrayList<>();

        Iterator<Document> it = coleccionFrutas.find().iterator();
        while(it.hasNext()){
            lista.add(documentToFruta(it.next()));
        }
        return lista;
    }
    public List<Verdura>getVerduras(){
        ArrayList<Verdura> lista = new ArrayList<>();

        Iterator<Document> it = coleccionVeruras.find().iterator();
        while(it.hasNext()){
            lista.add(documentToVerdura(it.next()));
        }
        return lista;

    }
    public List<Fruta> getFrutas(String text) {
        ArrayList<Fruta> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("origen", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionFrutas.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToFruta(iterator.next()));
        }

        return lista;
    }
    public List<Verdura>getVerduras(String text){
        ArrayList<Verdura>lista= new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("origen", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> it = coleccionVeruras.find().iterator();
        while (it.hasNext()){
            lista.add(documentToVerdura(it.next()));
        }
        return lista;
    }
    private Fruta documentToFruta(Document document) {
        Fruta unaFruta = new Fruta();
        unaFruta.setId(document.getObjectId("_id"));
        unaFruta.setNombre(document.getString("nombre"));
        unaFruta.setOrigen(document.getString("origen"));
        unaFruta.setPesoNeto(document.getDouble("peso"));
        unaFruta.setFechaCaducidad(Util.parsearFecha(document.getString("fechaCaducidad")));
        return unaFruta;
    }

    private Object frutaToDocument(Fruta unaFruta) {
        Document documento = new Document();
        documento.append("nombre", unaFruta.getNombre());
        documento.append("origen", unaFruta.getOrigen());
        documento.append("peso", unaFruta.getPesoNeto());
        documento.append("fechaCaducidad", Util.formatearFecha(unaFruta.getFechaCaducidad()));
        return documento;
    }

    public void modificarFruta(Fruta unaFruta) {
        coleccionFrutas.replaceOne(new Document("_id", unaFruta.getId()), frutaToDocument(unaFruta));
    }

    public void borrarFruta(Fruta unaFruta) {
        coleccionFrutas.deleteOne((Bson) frutaToDocument(unaFruta));
    }
    public void borrarVerdura(Verdura unaVerdura){
        coleccionVeruras.deleteOne((Bson) verduraToDocument(unaVerdura));
    }

    public void modificarVerdura(Verdura unaVerdura) {
        coleccionVeruras.replaceOne(new Document("id", unaVerdura.getId()),verduraToDocument(unaVerdura));
    }

    private Object verduraToDocument(Verdura unaVerdura) {
        Document documento= new Document();
        documento.append("nombre", unaVerdura.getNombre());
        documento.append("origen", unaVerdura.getOrigen());
        documento.append("peso", unaVerdura.getPesoNeto());
        documento.append("fechaCaducidad", Util.formatearFecha(unaVerdura.getFechaCaducidad()));
        return documento;
    }

    private Verdura documentToVerdura(Document document) {
        Verdura unaVerdura= new Verdura();
        unaVerdura.setId(document.getObjectId("_id"));
        unaVerdura.setNombre(document.getString("nombre"));
        unaVerdura.setOrigen(document.getString("origen"));
        unaVerdura.setPesoNeto(document.getDouble("peso"));
        unaVerdura.setFechaCaducidad(Util.parsearFecha(document.getString("fechaCaducidad")));
        return unaVerdura;

    }
}
