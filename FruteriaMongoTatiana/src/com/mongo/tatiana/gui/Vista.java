package com.mongo.tatiana.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.mongo.tatiana.base.Fruta;
import com.mongo.tatiana.base.Verdura;

import javax.swing.*;

public class Vista extends JFrame {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    //FRUTA//
    JTextField txtNombreFruta;
    JTextField txtPesoNeto;
    JTextField txtBuscar;
    JTextField txtOrigen;
    DatePicker fechaCaducidad;
    JList listFrutas;
    JButton btnNuevo;
    JButton btnModificar;
    JButton btnEliminar;


    //VERDURA//
    JTextField txtNombreVerdura;
    JTextField txtOrigenVerdura;
    JTextField txtBuscarVerdura;
    JTextField txtPesoVerdura;
    JList listVerdura;
    DatePicker fechaCaducidadVerdura;
    JButton btnNuevoVerdura;
    JButton btnModificarVerdura;
    JButton btnEliminarVerdura;

    DefaultListModel<Fruta> dlmFrutas;
    DefaultListModel<Verdura> dlmVerduras;



    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }

    private void inicializar() {
        dlmFrutas = new DefaultListModel<Fruta>();
        listFrutas.setModel(dlmFrutas);

        dlmVerduras= new DefaultListModel<Verdura>();
        listVerdura.setModel(dlmVerduras);
    }

    fechaCaducidad.getComponentToggleCalendarButton().setText("Calendario");
    fechaCaducidadVerdura.getComponentToggleCalendarButton().setText("Calendario");


}
